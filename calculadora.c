// Calculadora usando estrutura de pilha e com notação pós-fixada
#include <stdio.h>  // Usando: printf(), scanf()
#include <stdlib.h> // Usando: malloc(), free(), aToF()
#include <math.h>   // Usando: fAbsF(), roundF()

struct no{ // Define como é um nó da pilha
    float dado;
    struct no *anterior; // Ponteiro para o nó anterior
}typedef NO;

typedef NO* PILHA; // Define o tipo PILHA. O qual é um ponteiro para o nó que está no topo da pilha

// Cria uma pilha
PILHA* criarPilha(){
    PILHA* topoPilha = (PILHA*)malloc(sizeof(PILHA));
    if(topoPilha != NULL){ // Se foi alocado com sucesso
        *topoPilha = NULL; // deixe o conteúdo vazio
    }
    return topoPilha;
}

// Empilha e guarda dadoOrigem na pilha
int push(PILHA* topoPilha, float dadoOrigem){
    if(topoPilha == NULL) return 0; // Se não existe, erro

    NO* elemento = (NO*)malloc(sizeof(NO));
    if(elemento == NULL) return 0; // Se não foi possível alocar, erro

    elemento->dado = dadoOrigem; // Guarda informação dentro do nó
    elemento->anterior = *topoPilha; // elemento → [topoPilha = no_1] → no_2 ...
    *topoPilha = elemento; // Elemento vira topo da pilha: [topoPilha = elemento] → no_1 → no_2 ...
    
    return 1;
}

// Desempilha, resgata o conteúdo guardado e insere em dadoDestino
int pop(PILHA* topoPilha, float* dadoDestino){
    if(topoPilha == NULL || *topoPilha == NULL) return 0; // Se não existe ou topoPilha tem conteúdo vazio, retorna erro

    *dadoDestino = (*topoPilha)->dado;  // Preenche o dadoDestino
    NO *noAuxiliar = *topoPilha; // Guarda antigo topo da pilha
    *topoPilha = (*topoPilha)->anterior; // Topo da pilha passa a ser o elemento anterior

    free(noAuxiliar); // Libera antigo topo da pilha

    return 1;
}

// Calcula apenas 1 operação usando números da pilha
int calcular(PILHA* pilha, char operador){
    float numero1, numero2;
    int codigoRetornado=0;
    codigoRetornado += pop(pilha, &numero2); // Desempilha em numero2
    codigoRetornado += pop(pilha, &numero1); // Desempilha em numero1

    switch (operador){
        case '+':
            codigoRetornado += push(pilha, (numero1 + numero2)); // Empilha resultado da operação
            break;
        case '-':
            codigoRetornado += push(pilha, (numero1 - numero2)); // Empilha resultado da operação
            break;
        case '*':
            codigoRetornado += push(pilha, (numero1 * numero2)); // Empilha resultado da operação
            break;
        case '/':
           codigoRetornado +=  push(pilha, (numero1 / numero2)); // Empilha resultado da operação
            break;
        default:
            return -1; // Expressão apresenta erros
    }
    if(codigoRetornado == 3){ // Se verdadeiro,
        return 1; // todas as operações foram realisadas com sucesso
    }else{ // Se não,
        return codigoRetornado; // ocorreu erro durante operações
    }
}

// Verifica se é um operador válido
int isOperator(char texto){
    switch (texto){
        case '-':
        case '+':
        case '*':
        case '/':
            return 1; // É uma operador válido
        default:
            return 0; // Não é um operador válido
    }
}

// Verifica se uma string é um número
int isNumber(char string[]){
    int i;
    for(i = 0; string[i] != '\0'; i++){ // Percorre cada caractere da string
        // Aceita-se apenas: dígito [0-9], '-' ou '.'
        if((string[i] < '-' || string[i] > '9' || string[i] == '/')) return 0;
    }
    if(i <= 1 && string[0] == '-') return 0; // Excluí caracter '-' apenas
    return 1; // Verdadeiro ee todos os caracteres passaram no teste
}

int main(){
    PILHA* pilha = criarPilha();
    float resultado;
    char txt[5] = "\0";

    printf(
        "\x1B[1;32m======== Calculadora pós-fixada ==========\x1b[0m\n"
        "Insira expressão na notação pós-fixada\n"
        "separada por espaços e \x1b[1;35m;\x1b[0m ao final\n"
        ">: "
    );
    do{
        scanf(" %s", txt); // Lê até encontrar "espaço", "tabulação" ou "enter", porém não os inclui na string
        if(txt[0] != ';'){ // Se não for indicativa de fim da expressão
            if(isNumber(txt)){ // Verifica se é um número
                push(pilha, atof(txt)); // Empilha, convertendo de string para float
            }
            else if(isOperator(txt[0])){ // Verifica se é um operador matemático
                calcular(pilha, txt[0]); // tenta calcular usando o operador
            }
            else{ // Coso contrário
                printf("Erro no formato da expressão.\n");
                break;
            }
        }
    }while(txt[0] != ';' && txt[1] != ';'); // Se encontrar ';' termina a repetição

    if( pop(pilha, &resultado) ){ // pop() retorna 1 se foi executado com sucesso ou 0 se ocorreu erro
        if( fabsf(roundf(resultado) - resultado) <= 0.01f){ // Verifica casa decimais para
            printf("%.0f \n", resultado);
        }else{
            printf("%.2f \n", resultado);
        }
    }else{
        printf("Algo não ocorreu bem.\n");
    }
    return 0;
}
