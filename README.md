# Calculadora pós-fixada

Este é um exercício proposto na disciplina de **Estrutura de Dados 1** do [**Instituto de Informática**](https://inf.ufg.br/) da [**Universidade Federal de Goiás**](https://www.ufg.br/).

Consiste na implementação de uma calculadora usando estrutura de pilha e com notação [pós-fixada (ou polonesa inversa)](https://pt.wikipedia.org/wiki/Nota%C3%A7%C3%A3o_polonesa_inversa).

## Como compilar e executar?

Os requisitos são:
* [Git](https://git-scm.com/)
* [GNU Make](https://www.gnu.org/software/make/)
* Um Compilador para **C**
    * [GCC](https://gcc.gnu.org/) ou
    * [Clang](https://clang.llvm.org/) ou
    * [MinGW](https://www.mingw-w64.org/)

### Instalação das ferramentas
* No **Windows** utilizando [Chocolatey](https://chocolatey.org/install#individual).

Execute o comando no PowerShell
```powershell
> choco install git mingw make
```

* No **Ubuntu, Debian, Mint, PopOS**...</br>

Execute o comando no Terminal
```bash
$ sudo apt update && sudo apt install git build-essential
```

* No **MacOS** </br>

Instale [Command Line Tools](https://developer.apple.com/download/all/) ou [Xcode](https://developer.apple.com/download/all/)

### Compilando e executando
Basta clonar o repositório e utilizar o comando make para compilar
```bash
$ git clone https://gitlab.com/exercicios-estrutura-de-dados/calculadora-pos-fixada.git
$ cd calculadora-pos-fixada/
$ make
$ make run
```

## License
This project is licensed under the terms of the [MIT](https://choosealicense.com/licenses/mit/) license.
