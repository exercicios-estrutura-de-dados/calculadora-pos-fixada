# Variávies
CC = gcc
CFLAGS = -pedantic-errors -Wall

# Regra : dependências
all: calculadora.c
	$(CC) $(CFLAGS) calculadora.c -o calculadora

clean:
	rm calculadora

run:
	./calculadora

debug:
	make CFLAGS+=-g
